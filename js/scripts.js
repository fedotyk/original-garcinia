  // Initialization of height equalizing function
  $.fn.equalizeHeights = function() {
    var maxHeight = this.map(function(i, e) {
      return $(e).height();
    }).get();
    return this.height(Math.max.apply(this, maxHeight));
  };

$(document).ready(function() {

  /* Add browser name as class to the webpage body */
  var ua = navigator.userAgent.toLowerCase();
  if (ua.indexOf('chrome') != -1) {
    $('body').addClass('BrowserWebkit');
  } else if (ua.indexOf('safari') != -1) {
    $('body').addClass('BrowserSafari');
  } else if (ua.indexOf('msie') != -1) {
    var ver = parseInt(ua.split('msie')[1]);
    $('body').addClass('BrowserIE BrowserIE' + ver);
  } else {
    $('body').addClass('BrowserFirefox');
  }

  // Added class switcher. Will be used to highlight checked products
  $(".buy-product-page .radio").click(function() {
    $(this).addClass('checked');
    $(this).siblings().removeClass('checked');
  });

  // Height equalizing
  $(".qualify-order").equalizeHeights();
  $(".benefits-section .col-sm-6 div:first-child").equalizeHeights();
  
});
